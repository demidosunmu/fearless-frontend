import React,{ useEffect, useState } from "react";

function ConferenceForm(){
    const[location, setLocation] = useState([]);
    const [name, setName]= useState('');
    const [startDate, setStartDate] = useState('');
    const [endsDate, setEndsDate]= useState('');
    const [description, setDescription] = useState('');
    const [maxPresentations, setMaxPresentations] = useState('');
    const [maxAttendees, setMaxAttendees] =useState('');
    const [locations, setLocations] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value)
    }
    const handleStartDateChange = (event) => {
        const value = event.target.value;
        setStartDate(value)
    }
    const handlesEndsDateChange = (event) => {
        const value = event.target.value;
        setEndsDate(value)
    }
    const handlesDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value)
    }
    const handlesMaxPresentationsChange = (event) => {
        const value = event.target.value;
        setMaxPresentations(value)
    }
    const handlesMaxAttendeesChange = (event) => {
        const value = event.target.value;
        setMaxAttendees(value)
    }

    const handlesLocationsChange = (event) => {
        const value = event.target.value;
        setLocations(value)
    }

    const fetchData = async () => {
        const url = "http://localhost:8000/api/locations/";
        const response = await fetch(url)
        if (response.ok){
            const data = await response.json();
            setLocation(data.location)
        }

    }

    const handleSubmit = async (event) =>{
        event.preventDefault();

        const data = {};
        data.name = name;
        data.starts = startDate;
        data.ends = endsDate;
        data.description= description;
        data.max_presentations=maxPresentations;
        data.max_attendees=maxAttendees;
        data.location=locations;
        console.log(data);


        const conferenceUrl = "http://localhost:8000/api/conferences/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            header: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok){
            const newConference = await response.json();
            console.log(newConference)
            setName('');
            setStartDate('');
            setEndsDate('');
            setDescription('');
            setMaxPresentations('');
            setMaxAttendees('');
            setLocations('');
        }
    }

    useEffect(()=> {
        fetchData();
    }, [])

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit}id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange= {handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={name}/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange= {handleStartDateChange} placeholder="mm/dd/yy"  required type="date" name="starts" id="starts" min="2018-01-01" max="2024-12-31" className="form-control" value={startDate}/>
                <label htmlFor="starts">Start date</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlesEndsDateChange} placeholder="mm/dd/yy"  required type="date" name="ends" id="ends"  min="2018-01-01" max="2024-12-31" className="form-control" value={endsDate}/>
                <label htmlFor="ends">Ends date</label>
              </div>
              <div className="mb-3">
                <label htmlFor="Description" className="form-label">Description</label>
                <textarea onChange={handlesDescriptionChange} className="form-control" name="description" id="description" rows="3" value={description}></textarea>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlesMaxPresentationsChange} placeholder="Max_presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" value={maxPresentations}/>
                <label htmlFor="max_presentations">Maximum presentation</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlesMaxAttendeesChange} placeholder="Max_attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" value={maxAttendees}/>
                <label htmlFor="max_attendees">Maximum attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={handlesLocationsChange} required name="location" id="location" className="form-select" value={locations}>
                  <option value="">Choose a location</option>
                    {location.map(location => {
                        return (
                        <option key={location.id} value ={location.id}>
                            {location.name}
                        </option>
                        );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}
export default ConferenceForm
